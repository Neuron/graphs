package at.plaz.graphs;

import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public class SimpleNodeFactory implements NodeFactory<SimpleNode> {
    private static final NodeFactory<SimpleNode> singleton = new SimpleNodeFactory();

    public static NodeFactory<SimpleNode> singleton() {
        return singleton;
    }

    @Override
    public SimpleNode createNode(DoubleTuple center) {
        return new SimpleNode(center);
    }
}
