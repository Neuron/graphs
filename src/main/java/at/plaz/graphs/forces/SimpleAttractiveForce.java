package at.plaz.graphs.forces;

import at.plaz.tuples.DoubleTuple;
import at.plaz.tuples.DoubleTuples;

import static at.plaz.tuples.DoubleTuples.*;

/**
 * Created by Georg Plaz.
 */
public class SimpleAttractiveForce<A extends Locatable, B extends LocatableForceReceiver> extends AbstractDirectedForce<A, B> {
//    private SimpleRepulsiveForce repellent;
    private double intensityScale;
    private double springConstant = 0.001;
    private DoubleTuple positionScalar;
    public SimpleAttractiveForce(A source, double intensityScale) {
        this(source, intensityScale, ONES);
    }

    public SimpleAttractiveForce(A source, double intensityScale, DoubleTuple positionScalar) {
        super(source);
        this.intensityScale = intensityScale;
        this.positionScalar = positionScalar;
    }

    @Override
    public double getForceIntensity(B object) {
        DoubleTuple delta = DoubleTuples.multiply(subtract(getSource().location().get(), object.location().get()), positionScalar);
        double distance = delta.length();
        return distance * springConstant * intensityScale;
    }

//    public void setDimensionalScale(double equilibriumDistance) {
//        this.equilibriumDistance = equilibriumDistance;
//    }
}
