package at.plaz.graphs.forces;

import at.plaz.graphs.Node;
import at.plaz.graphs.value.*;
import at.plaz.tuples.DoubleTuple;
import at.plaz.tuples.DoubleTuples;

import static at.plaz.tuples.DoubleTuples.multiply;
import static at.plaz.tuples.DoubleTuples.subtract;

/**
 * Created by Georg Plaz.
 */
public class EquilibriumForce<N extends Node> extends AbstractNodeForce<N, N> {
    private SimpleDoubleTupleProperty positionScalar = new SimpleDoubleTupleProperty(DoubleTuples.ONES);
    private DoubleProperty intensity = new SimpleDoubleProperty(1);
    private DoubleProperty repulsivePower = new SimpleDoubleProperty(2);
    private DoubleProperty attractivePower = new SimpleDoubleProperty(2);
    private DoubleProperty equilibriumPosition = new SimpleDoubleProperty(100);

    public EquilibriumForce(N source) {
        super(source);
    }

    @Override
    public double getForceIntensity(N other) {
        DoubleTuple target = other.location().get();
        DoubleTuple sourceLocation = getSource().location().get();
        DoubleTuple delta = DoubleTuples.multiply(DoubleTuples.subtract(target, sourceLocation), positionScalar.get());
        double distance = delta.length();
        double d = distance / getEquilibriumPosition(other, distance);
        double repulsion = - Math.pow(d, -repulsivePower.get());
        if (getSource().graph().get().isLinked(getSource(), other)) {
            return - (Math.pow(d, attractivePower.get()) + repulsion) * intensity.get();
        } else {
            return - repulsion * intensity.get();
        }
    }

    public double getEquilibriumPosition(N other, double distance) {
        return equilibriumPosition().get();
    }

    public DoubleProperty intensity() {
        return intensity;
    }

    public DoubleProperty repulsivePower() {
        return repulsivePower;
    }

    public DoubleProperty attractivePower() {
        return attractivePower;
    }

    public DoubleProperty equilibriumPosition() {
        return equilibriumPosition;
    }

    public SimpleDoubleTupleProperty positionScalar() {
        return positionScalar;
    }
}
