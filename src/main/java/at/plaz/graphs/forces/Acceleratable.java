package at.plaz.graphs.forces;

import at.plaz.graphs.value.DoubleTupleProperty;
import at.plaz.tuples.DoubleTuple;
import at.plaz.tuples.DoubleTuples;

/**
 * Created by Georg Plaz.
 */
public interface Acceleratable extends Accelerating, Movable, ForceReceiver {
    DoubleTupleProperty speed();

    @Override
    default void receiveForce(DoubleTuple force) {
        speed().set(DoubleTuples.add(speed().get(), force));
    }
}
