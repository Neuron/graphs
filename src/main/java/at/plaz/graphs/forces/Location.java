package at.plaz.graphs.forces;

import at.plaz.graphs.value.DoubleTupleValue;
import at.plaz.graphs.value.SimpleDoubleTupleProperty;
import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public class Location extends SimpleDoubleTupleProperty implements Locatable {
    public Location() {
    }

    public Location(DoubleTuple value) {
        super(value);
    }

    public Location(double x, double y) {
        super(x, y);
    }

    @Override
    public DoubleTupleValue location() {
        return this;
    }
}
