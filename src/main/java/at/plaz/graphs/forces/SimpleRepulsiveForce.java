package at.plaz.graphs.forces;

import at.plaz.tuples.DoubleTuple;
import at.plaz.tuples.DoubleTuples;

import static at.plaz.tuples.DoubleTuples.ONES;
import static at.plaz.tuples.DoubleTuples.subtract;

/**
 * Created by Georg Plaz.
 */
public class SimpleRepulsiveForce<A extends Locatable, B extends LocatableForceReceiver> extends AbstractDirectedForce<A, B> {
    //    private SimpleRepulsiveForce repellent;
    private double dimensionalScale;
    private DoubleTuple positionScalar;

    public SimpleRepulsiveForce(A source, double dimensionalScale) {
        this(source, ONES, dimensionalScale);
    }

    public SimpleRepulsiveForce(A source, DoubleTuple positionScalar, double dimensionalScale) {
        super(source);
        this.positionScalar = positionScalar;
        this.dimensionalScale = dimensionalScale;
    }

    @Override
    public double getForceIntensity(B object) {
        DoubleTuple delta = DoubleTuples.multiply(subtract(getSource().location().get(), object.location().get()), positionScalar);
        double distance = delta.length();
        return (-dimensionalScale/(Math.pow(distance / dimensionalScale, 2)));
    }
}

