package at.plaz.graphs.forces;

import at.plaz.graphs.DirectedForce;

/**
 * Created by Georg Plaz.
 */
public abstract class AbstractDirectedForce<A extends Locatable, B extends LocatableForceReceiver> implements DirectedForce<A, B> {
    private A source;

    public AbstractDirectedForce(A source) {
        this.source = source;
    }

    @Override
    public A getSource() {
        return source;
    }
}
