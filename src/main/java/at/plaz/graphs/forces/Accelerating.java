package at.plaz.graphs.forces;

import at.plaz.graphs.value.DoubleTupleValue;

/**
 * Created by Georg Plaz.
 */
public interface Accelerating extends Locatable {
    DoubleTupleValue speed();
}
