package at.plaz.graphs.forces;

import at.plaz.graphs.Force;
import at.plaz.tuples.DoubleTuples;

/**
 * Created by Georg Plaz.
 */
public class Forces {
    public static final Force<?> RELAXED_FORCE = object -> DoubleTuples.ZEROS;
    @SuppressWarnings("unchecked")
    public static <A extends ForceReceiver> Force<A> getRelacedForce() {return (Force<A>) RELAXED_FORCE;}


//    public static <M extends Node, N extends Node> NodeForce<M, N> getUnlinkedForce(M source, double equilibriumPoint, double intensity, double power) {
//        return new ForceMultiply<>(new ForceMagnitudePower<>(new ForceMultiply<>(new SimpleRepulsiveForce<>(equilibriumPoint), -1), power), -intensity);
//    }
//
//    public static <N extends Node> NodeForce<N> getLinkedForce(double equilibriumPoint, double intensity, double power) {
//        NodeForce<N> disconnectedForce = getUnlinkedForce(equilibriumPoint, intensity, power);
//        NodeForce<N> attractiveForce = new ForceMagnitudePower<>(new SimpleAttractiveForce<>(intensity), power);
//        return new AdditiveForce<>(attractiveForce, disconnectedForce);
//    }
}
