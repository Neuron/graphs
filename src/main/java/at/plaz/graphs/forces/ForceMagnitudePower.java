package at.plaz.graphs.forces;

import at.plaz.graphs.DirectedForce;

/**
 * Created by Georg Plaz.
 */
public class ForceMagnitudePower<A extends Locatable, B extends LocatableForceReceiver> implements DirectedForce<A, B> {
    private DirectedForce<A, B> toModify;
    private double power;

    public ForceMagnitudePower(DirectedForce<A, B> toModify, double power) {
        this.toModify = toModify;
        this.power = power;
    }

    @Override
    public double getForceIntensity(B object) {
        return Math.pow(toModify.getForceIntensity(object), power);
    }

    @Override
    public A getSource() {
        return toModify.getSource();
    }
}
