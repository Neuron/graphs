package at.plaz.graphs.forces;

import at.plaz.graphs.value.DoubleTupleValue;

/**
 * Created by Georg Plaz.
 */
public interface ForceSource {
    DoubleTupleValue location();
}
