package at.plaz.graphs.forces;

import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public interface Movable extends Locatable {
    void move(DoubleTuple delta);
}
