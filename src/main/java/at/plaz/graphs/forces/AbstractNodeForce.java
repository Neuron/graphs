package at.plaz.graphs.forces;

import at.plaz.graphs.Node;
import at.plaz.graphs.NodeForce;

/**
 * Created by Georg Plaz.
 */
public abstract class AbstractNodeForce<M extends Node, N extends Node> implements NodeForce<M, N> {
    private M source;

    protected AbstractNodeForce(M source) {
        this.source = source;
    }

//    @Override
//    public void actOn(N receiver) {
//        if (receiver != source) {
//            DoubleTuple force = forceOn(receiver);
//            receiver.receiveForce(DoubleTuples.divide(force, 2));
//            getSource().receiveForce(DoubleTuples.divide(force, -2));
//        }
//    }

    @Override
    public M getSource() {
        return source;
    }
}
