package at.plaz.graphs.forces;

import at.plaz.graphs.value.DoubleProperty;
import at.plaz.graphs.value.SimpleDoubleProperty;
import at.plaz.tuples.DoubleTuple;
import at.plaz.tuples.DoubleTuples;

/**
 * Created by Georg Plaz.
 */
public class CircularContainerForce<A extends Locatable, B extends LocatableForceReceiver> extends AbstractDirectedForce<A, B> {
    private DoubleProperty slope;
    private DoubleProperty radius;

    public CircularContainerForce(A center, double radius, double slope) {
        super(center);
        this.slope = new SimpleDoubleProperty(slope);
        this.radius = new SimpleDoubleProperty(radius);
    }

    @Override
    public double getForceIntensity(B other) {
        DoubleTuple target = getSource().location().get();
        DoubleTuple second = other.location().get();
        DoubleTuple delta = DoubleTuples.subtract(target, second);
        double distance = delta.length();
        if (distance < radius.value()) {
            return 0;
        } else {
            return -slope.value();
        }
    }

    public DoubleProperty slope() {
        return slope;
    }

    public DoubleProperty radius() {
        return radius;
    }
}
