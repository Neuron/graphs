package at.plaz.graphs.forces;

import at.plaz.graphs.Force;
import at.plaz.tuples.DefaultDoubleTupleF;
import at.plaz.tuples.DoubleTuple;
import at.plaz.tuples.DoubleTupleF;
import at.plaz.tuples.DoubleTuples;

/**
 * Created by Georg Plaz.
 */
public class ForceMultiply<A extends ForceReceiver> implements Force<A> {
    private Force<A> toModify;
    private DoubleTupleF factor;

    public ForceMultiply(Force<A> toModify, double dx, double dy) {
        this(toModify, new DefaultDoubleTupleF(dx, dy));
    }

    public ForceMultiply(Force<A> toModify, DoubleTuple factor) {
        this.toModify = toModify;
        this.factor = factor.toFinal();
    }

    @Override
    public DoubleTuple forceOn(A object) {
        return DoubleTuples.multiply(toModify.forceOn(object), factor);
    }
}
