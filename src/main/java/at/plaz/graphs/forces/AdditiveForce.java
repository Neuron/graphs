package at.plaz.graphs.forces;

import at.plaz.graphs.Force;
import at.plaz.tuples.DoubleTuple;
import at.plaz.tuples.DoubleTuples;

/**
 * Created by Georg Plaz.
 */
public class AdditiveForce<A extends ForceReceiver> implements Force<A> {
    private final Force<A> firstForce;
    private final Force<A> secondForce;

    public AdditiveForce(Force<A> firstForce, Force<A> secondForce) {
        this.firstForce = firstForce;
        this.secondForce = secondForce;
    }

    @Override
    public DoubleTuple forceOn(A object) {
        return DoubleTuples.add(firstForce.forceOn(object), secondForce.forceOn(object));
    }

    public Force<A> getFirstForce() {
        return firstForce;
    }

    public Force<A> getSecondForce() {
        return secondForce;
    }
}
