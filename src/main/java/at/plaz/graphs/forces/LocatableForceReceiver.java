package at.plaz.graphs.forces;

/**
 * Created by Georg Plaz.
 */
public interface LocatableForceReceiver extends ForceReceiver, Locatable {
}
