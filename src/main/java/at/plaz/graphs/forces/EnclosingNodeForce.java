package at.plaz.graphs.forces;

import at.plaz.graphs.DirectedForce;
import at.plaz.graphs.Node;

/**
 * Created by Georg Plaz.
 */
public class EnclosingNodeForce<M extends Node, N extends Node> extends AbstractNodeForce<M, N> {
    private DirectedForce<M, N> force;

    protected EnclosingNodeForce(M source, DirectedForce<M, N> force) {
        super(source);
        this.force = force;
    }

    @Override
    public double getForceIntensity(N object) {
        return force.getForceIntensity(object);
    }
}
