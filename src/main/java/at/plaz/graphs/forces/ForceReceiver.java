package at.plaz.graphs.forces;

import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public interface ForceReceiver {
    void receiveForce(DoubleTuple force);
}
