package at.plaz.graphs;

import at.plaz.graphs.forces.Locatable;
import at.plaz.graphs.forces.LocatableForceReceiver;
import at.plaz.tuples.DoubleTuple;
import at.plaz.tuples.DoubleTuples;

import static at.plaz.tuples.DoubleTuples.*;

/**
 * Created by Georg Plaz.
 */
public interface DirectedForce<A extends Locatable, B extends LocatableForceReceiver> extends Force<B> {
    A getSource();

    default DoubleTuple forceOn(B object) {
        DoubleTuple delta = subtract(object.location().get(), getSource().location().get());
        if (delta.length() == 0){
            return DoubleTuples.ZEROS;
        } else {
            return DoubleTuples.multiply(DoubleTuples.normalize(delta), getForceIntensity(object));
        }
    }

    double getForceIntensity(B object);
}
