package at.plaz.graphs;

import at.plaz.graphs.forces.ForceMultiply;
import at.plaz.graphs.forces.ForceReceiver;
import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public interface Force<A extends ForceReceiver> {
    DoubleTuple forceOn(A object);

    default Force<A> multiply(DoubleTuple value) {
        return new ForceMultiply<A>(this, value);
    }

    default Force<A> multiply(double dx, double dy) {
        return new ForceMultiply<>(Force.this, dx, dy);
    }

    default void actOn(A receiver) {
        receiver.receiveForce(forceOn(receiver));
    }
}
