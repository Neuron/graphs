package at.plaz.graphs;

/**
 * Created by Georg Plaz.
 */
public interface NodeAddedListener<A extends Node> {
    void added(A node);
}
