package at.plaz.graphs;

import at.plaz.tuples.DefaultSymTupleF;

/**
 * Created by Georg Plaz.
 */
public class SimpleLink<N extends Node> extends DefaultSymTupleF<N> implements Link<N> {
    public SimpleLink(N first, N second) {
        super(first, second);
    }
}
