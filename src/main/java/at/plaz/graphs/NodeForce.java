package at.plaz.graphs;

import at.plaz.tuples.DoubleTuple;
import at.plaz.tuples.DoubleTuples;

/**
 * Created by Georg Plaz.
 */
public interface NodeForce<M extends Node, N extends Node> extends DirectedForce<M, N> {
    @Override
    default void actOn(N receiver) {
        if (receiver != getSource()) {
            DoubleTuple force = forceOn(receiver);
            receiver.receiveForce(DoubleTuples.divide(force, 2));
            getSource().receiveForce(DoubleTuples.divide(force, -2));
        }
    }
}
