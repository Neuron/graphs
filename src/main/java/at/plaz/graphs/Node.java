package at.plaz.graphs;

import at.plaz.graphs.forces.Acceleratable;
import at.plaz.graphs.forces.LocatableForceReceiver;
import at.plaz.graphs.value.BooleanProperty;
import at.plaz.graphs.value.DoubleTupleProperty;
import at.plaz.graphs.value.Property;
import at.plaz.tuples.DoubleTuple;


/**
 * Created by Georg Plaz.
 */
public interface Node extends Acceleratable, LocatableForceReceiver {
    DoubleTupleProperty location();

    DoubleTupleProperty speed();

    BooleanProperty fixed();

    void move(DoubleTuple delta);

    Property<Graph> graph();

    BooleanProperty linkable();
}
