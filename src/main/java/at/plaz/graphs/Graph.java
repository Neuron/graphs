package at.plaz.graphs;

import at.plaz.graphs.forces.EquilibriumForce;
import at.plaz.graphs.value.DoubleProperty;
import at.plaz.graphs.value.SimpleDoubleProperty;
import at.plaz.graphs.value.Property;
import at.plaz.tuples.DoubleTuples;

import java.util.*;

import static at.plaz.tuples.DoubleTuples.*;

/**
 * Created by Georg Plaz.
 */
public class Graph<N extends Node, L extends Link<N>> {
    //    private NodeForce<N> connectedForce;
//    private NodeForce<N> disconnectedForce;
//    private NodeForce<N> defaultForce;
    //    private NodeForce<N> attractiveForce;
    private Set<N> nodes = new LinkedHashSet<>();
    private Physics physics;
    private List<NodeAddedListener<N>> nodeAddedListeners = new LinkedList<>();
    private List<LinkAddedListener<N, L>> linkAddedListeners = new LinkedList<>();
    private List<NodeRemovedListener<N>> nodeRemovedListeners = new LinkedList<>();
    private List<LinkRemovedListener<N, L>> linkRemovedListeners = new LinkedList<>();
    private Map<N, Map<N, L>> linksMapped = new LinkedHashMap<>();
    private Set<L> links = new LinkedHashSet<>();
    private final SimpleDoubleProperty equilibrium;
    private final SimpleDoubleProperty intensityScale;
    private final SimpleDoubleProperty friction;
    private final SimpleDoubleProperty forcePower;
    private Set<Force<? super N>> forces = new HashSet<>();

    public Graph() {
        this(new Physics());
    }

    public Graph(Physics physics) {
        this.physics = physics;
        this.equilibrium = new SimpleDoubleProperty(100.);
        this.intensityScale = new SimpleDoubleProperty(0.1);
        this.forcePower = new SimpleDoubleProperty(3);
        this.friction = new SimpleDoubleProperty(0.9);
    }

    public EquilibriumForce createEquilibriumForce(Node node) {
        EquilibriumForce equilibriumForce = new EquilibriumForce(node);
        equilibriumForce.attractivePower().bind(forcePower);
        equilibriumForce.repulsivePower().bind(forcePower);
        equilibriumForce.intensity().bind(intensityScale);
        equilibriumForce.equilibriumPosition().bind(equilibrium);
        return equilibriumForce;
    }

    public void removeForce(Force<? super N> force) {
        forces.remove(force);
    }

    public void addForce(Force<? super N> force) {
        forces.add(force);
    }

    public void addNodeAddedListener(NodeAddedListener<N> listener) {
        nodeAddedListeners.add(listener);
    }

    public void addNodeRemovedListener(NodeRemovedListener<N> listener) {
        nodeRemovedListeners.add(listener);
    }

    public void addLinkAddedListener(LinkAddedListener<N, L> listener) {
        linkAddedListeners.add(listener);
    }

    public void addLinkRemovedListener(LinkRemovedListener<N, L> listener) {
        linkRemovedListeners.add(listener);
    }

    public void update() {
//        Map<N, DoubleTuple> deltas = new HashMap<>();
//        Map<N, Set<N>> calculated = new LinkedHashMap<>();
        Set<N> nodes = new LinkedHashSet<>(this.nodes);
//        for (N node : nodes) {
//            calculated.put(node, new HashSet<>());
////            deltas.put(node, DoubleTuples.ZEROS);
//        }
        for (Force<? super N> force : new LinkedList<>(forces)) {
            for (N node : nodes) {
                force.actOn(node);
//                DoubleTuple effect = force.forceOn(node);
//                node.speed().set(add(node.speed().get(), effect));
            }
        }
//        for (N node : nodes) {
//            for(N other : nodes) {
////                if (!calculated.get(node).contains(other)) {
//                    if (other != node) {
//                        DoubleTuple vec = subtract(other.location().get(), node.location().get());
//                        double distance = vec.length();
//                        if (distance > 0) {
//                            NodeForce<N> force = node.getForce(other);
//                            double attraction = force.getAttraction(node, other)/2;
//                            DoubleTupleF delta = multiply(normalize(vec), attraction);
////                            if (!node.fixed().get() && !other.fixed().get()) {
////                                delta = divide(delta, 2);
////                            }
//                            if (!node.fixed().get()) {
//                                node.speed().add(delta);
//                            }
//                            if (!other.fixed().get()) {
//                                other.speed().add(multiply(delta, -1));
//                            }
//                        }
//                    }
////                    calculated.get(other).add(node);
////                }
//            }
//        }
        for (N node : nodes) {
            if (node.fixed().get()) {
                node.speed().set(DoubleTuples.ZEROS);
            }
            node.move(node.speed().get());
            node.speed().set(multiply(node.speed().get(), friction.get()));
        }
//        for (N node : nodes) {
//            if (!node.fixed().get()) {
//                DoubleTuple delta = deltas.get(node);
//                node.move(delta);
//            }
//        }
    }

    public void establishLink(L link) {
        link(link.getFirst(), link.getSecond(), link);
    }

    public Link link(N first, N second, L link) {
        if (isLinked(first, second)) {
            throw new AlreadyLinkedException("Can't link nodes which are already linked.");
        } else {
            if (!first.linkable().value()){
                throw new NotLinkableException("Node "+first+" is not linkable!", first);
            }
            if (!second.linkable().value()){
                throw new NotLinkableException("Node "+second+" is not linkable!", second);
            }
            linksMapped.get(first).put(second, link);
            linksMapped.get(second).put(first, link);
            links.add(link);
            for (LinkAddedListener<N, L> linkAddedListener : linkAddedListeners) {
                linkAddedListener.added(link);
            }
            return link;
        }
    }

    public Set<L> getLinks() {
        return links;
    }

    public Collection<L> getLinks(N forNode) {
        return linksMapped.get(forNode).values();
    }

//    public NodeForce<N> getForce(N node, N other) {
//        return node.getForce(other);
//    }

    public Property<Double> DimensionalScale() {
        return equilibrium;
    }

    public Property<Double> IntensityScale() {
        return intensityScale;
    }

    public Physics getPhysics() {
        return physics;
    }

    public void addNode(N node) {
        if (!nodes.contains(node)) {
            nodes.add(node);
            node.graph().set(this);
            linksMapped.put(node, new LinkedHashMap<>());
            for (NodeAddedListener<N> listener : nodeAddedListeners) {
                listener.added(node);
            }
        }
    }

    public Set<N> getNodes() {
        return nodes;
    }

//    public NodeForce<N> getConnectedForce() {
//        return connectedForce;
//    }
//
//    public void setConnectedForce(NodeForce<N> connectedForce) {
//        this.connectedForce = connectedForce;
//    }
//
//    public NodeForce<N> getDisconnectedForce() {
//        return disconnectedForce;
//    }
//
//    public void setDisconnectedForce(NodeForce<N> disconnectedForce) {
//        this.disconnectedForce = disconnectedForce;
//    }

    public L getLink(Node node, Node neighbour) {
        return linksMapped.get(node).get(neighbour);
    }

    public boolean isLinked(Node node, Node neighbour) {
        return linksMapped.containsKey(node) && linksMapped.get(node).containsKey(neighbour);
    }

    public L unlink(Node node, Node neighbour) {
        if (!isLinked(node, neighbour)) {
            throw new RuntimeException("Can't unlink nodes which are not linked!");
        }
        L link = linksMapped.get(node).remove(neighbour);
        linksMapped.get(neighbour).remove(node);
        links.remove(link);
        for (LinkRemovedListener<N, L> listener : linkRemovedListeners) {
            listener.removed(link);
        }
        return link;
    }

    public Set<N> getNeighbours(Node node) {
        return linksMapped.containsKey(node) ? linksMapped.get(node).keySet() : Collections.emptySet();
    }

    public int countNeigbours(Node node) {
        return linksMapped.containsKey(node) ? linksMapped.get(node).size() : 0;
    }

    public void remove(Node node) {
        for (N neighbour : new LinkedList<>(getNeighbours(node))) {
            unlink(node, neighbour);
        }
        nodes.remove(node);
        for (NodeRemovedListener listener : nodeRemovedListeners) {
            listener.removed(node);
        }
    }

    public void removeAll(List<N> toRemove) {
        for (N node : toRemove) {
            remove(node);
        }
    }

    public void unlink(N node) {
        for (N neighbour : new LinkedList<>(getNeighbours(node))) {
            unlink(node, neighbour);
        }
    }

    public DoubleProperty equilibriumDistance() {
        return equilibrium;
    }

    public DoubleProperty intensityScale() {
        return intensityScale;
    }

    public DoubleProperty friction() {
        return friction;
    }

    public static class AlreadyLinkedException extends RuntimeException {
        public AlreadyLinkedException() {
        }

        public AlreadyLinkedException(String message) {
            super(message);
        }

        public AlreadyLinkedException(String message, Throwable cause) {
            super(message, cause);
        }

        public AlreadyLinkedException(Throwable cause) {
            super(cause);
        }

        public AlreadyLinkedException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
        }
    }
}
