package at.plaz.graphs.persistence;

import java.io.File;

/**
 * Created by Georg Plaz.
 */
public interface FileDAO<A> extends DAO<A, File> {
}
