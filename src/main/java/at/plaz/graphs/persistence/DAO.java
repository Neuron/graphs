package at.plaz.graphs.persistence;

/**
 * Created by Georg Plaz.
 */
public interface DAO<A, K> {
    A read(K key) throws ReadException;
    void write(A val, K key) throws WriteException;

    class WriteException extends Exception {
        public WriteException() {
        }

        public WriteException(String message) {
            super(message);
        }

        public WriteException(String message, Throwable cause) {
            super(message, cause);
        }

        public WriteException(Throwable cause) {
            super(cause);
        }

        public WriteException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
        }
    }

    class ReadException extends Exception {
        public ReadException() {
        }

        public ReadException(String message) {
            super(message);
        }

        public ReadException(String message, Throwable cause) {
            super(message, cause);
        }

        public ReadException(Throwable cause) {
            super(cause);
        }

        public ReadException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
            super(message, cause, enableSuppression, writableStackTrace);
        }
    }
}
