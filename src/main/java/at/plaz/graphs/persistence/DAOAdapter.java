package at.plaz.graphs.persistence;

import at.plaz.graphs.value.StorableNode;

/**
 * Created by Georg Plaz.
 */
public interface DAOAdapter<A> {
    String getKey();

    Class<A> getAdapterClass();

    StorableNode getStorables(A node);
}
