package at.plaz.graphs.persistence;

import at.plaz.graphs.*;
import at.plaz.graphs.value.GraphAdapter;
import at.plaz.graphs.value.StorableNode;
import at.plaz.tuples.DoubleTuples;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Georg Plaz.
 */
public class GraphXMLDAO<N extends Node, L extends Link<N>> extends AbstractXMLDAO<Graph<N, L>> implements GraphDAO<N, L>{
//    private Converter<String, NodeFactory<N>> fromXMLTagConverter = value -> value.getClass().getSimpleName();

    public static final String SCALE_ATTRIBUTE_KEY = "scale";
    public static final String GRAPH_ELEMENT_KEY = "graph";
    public static final String NODES_ELEMENT_KEY = "nodes";
    public static final String LINKS_ELEMENT_KEY = "links";
    public static final String LINK_ELEMENT_KEY = "link";
//    public static final String NODE_ELEMENT_KEY = "node";
//    public static final String POSITION_ATTRIBUTE_KEY = "position";
    public static final String LINK_ID_ATTRIBUTE_KEY = "link_id";
    public static final String FIRST_NODE_ID_ATTRIBUTE_KEY = "first_node_id";
    public static final String SECOND_NODE_ID_ATTRIBUTE_KEY = "second_node_id";
    public static final String ROOT_ELEMENT_KEY = "force_directed_graph";

    private LinkFactory<N, L> linkFactory;
    private Map<String, NodeAdapter<? extends N>> nodeAdaptersByTag = new HashMap<>();
    private Map<Class, NodeAdapter<? extends N>> nodeAdaptersByClass = new HashMap<>();
    private GraphAdapter<N, L> graphAdapter;

    public GraphXMLDAO(LinkFactory<N, L> linkFactory) {
        this.linkFactory = linkFactory;
        graphAdapter = new GraphAdapter<>("graph",  Graph.class);
    }

    public void addNodeAdapter(NodeAdapter<? extends N> nodeAdapter) {
        this.nodeAdaptersByTag.put(nodeAdapter.getKey(), nodeAdapter);
        this.nodeAdaptersByClass.put(nodeAdapter.getAdapterClass(), nodeAdapter);
    }

    public void setLinkFactory(LinkFactory<N, L> linkFactory) {
        this.linkFactory = linkFactory;
    }

    public void write(Graph<N, L> graph, File file) throws WriteException{
        Document document = createDocument();
        Element root = document.createElement(ROOT_ELEMENT_KEY);
        document.appendChild(root);
        Element graphElement = appendTo(root, graphAdapter.getStorables(graph), document);
        Element nodesElement = createChildElement(NODES_ELEMENT_KEY, graphElement, document);
        Element linksElement = createChildElement(LINKS_ELEMENT_KEY, graphElement, document);

//        writeGraph(graphElement, graph, document);

        int idCounter = 0;
        Map<N, String> nodeLinkIds = new HashMap<>();
        for (N node : graph.getNodes()) {
            String idString = String.valueOf(idCounter++);
            NodeAdapter nodeAdapter = nodeAdaptersByClass.get(node.getClass());
            appendTo(nodesElement, storables(nodeAdapter, node), document).setAttribute(LINK_ID_ATTRIBUTE_KEY, idString);
            nodeLinkIds.put(node, idString);
        }
        for (L link : graph.getLinks()) {
            Element linkElement = createChildElement(LINK_ELEMENT_KEY, linksElement, document);
            writeLink(linkElement, link, nodeLinkIds, document);
        }
        persistDocument(document, file);
    }

    private StorableNode storables(NodeAdapter<?> nodeAdapter, N node) {
        @SuppressWarnings("unchecked")
        NodeAdapter<N> adapter = (NodeAdapter<N>) nodeAdapter;
        return adapter.getStorables(node);
    }

    public void writeGraph(Element element, Graph<N, L> graph, Document document) {
        appendTo(element, graphAdapter.getStorables(graph), document);
//        element.setAttribute(SCALE_ATTRIBUTE_KEY, String.valueOf(graph.getDimensionalScale()));
    }

//    public void writeNode(Element element, N node, Map<N, String> nodeLinkIds, int nodeIndex, Document document) {
//        String linkId = String.valueOf(nodeIndex);
//        element.setAttribute(LINK_ID_ATTRIBUTE_KEY, linkId);
//        for (Storable storable : node.getStorables().values()) {
//            if (storable instanceof StorableLeaf) {
//                StorableLeaf storableLeaf = (StorableLeaf) storable;
//                Element leaf = createChildElement(storableLeaf.key(), element, document);
//                leaf.setAttribute(VALUE_ATTRIBUTE_KEY, storableLeaf.getValueAsString());
//            }
//        }
////        element.setAttribute(POSITION_ATTRIBUTE_KEY, node.location().get().toString());
//        nodeLinkIds.put(node, linkId);
//    }

    public void writeLink(Element element, Link<N> link, Map<N, String> nodeIds, Document document) throws WriteException {
        String firstNodeId = nodeIds.get(link.getFirst());
        if (firstNodeId == null) {
            throw new WriteException("Trying to write link "+link+" but failed, because the first node was not present in the graph.");
        }
        element.setAttribute(FIRST_NODE_ID_ATTRIBUTE_KEY, firstNodeId);
        String secondNodeId = nodeIds.get(link.getSecond());
        if (secondNodeId == null) {
            throw new WriteException("Trying to write link "+link+" but failed, because the second node was not present in the graph.");
        }
        element.setAttribute(SECOND_NODE_ID_ATTRIBUTE_KEY, secondNodeId);
    }

    @Override
    public Graph<N, L> read(File file) throws ReadException{
        try {
            Element rootElement = getRootElement(file);
            Element graphElement = getElementByTagName(rootElement, GRAPH_ELEMENT_KEY);
            Graph<N, L> graph = readGraph(graphElement);
//            graph.setDimensionalScale(readScale(graphElement));
            return read(graph, graphElement);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            throw new ReadException(e);
        }
    }

    public Graph<N, L> read(File file, Graph<N, L> graph) throws ReadException{
        try {
            Element rootElement = getRootElement(file);
            Element graphElement = getElementByTagName(rootElement, GRAPH_ELEMENT_KEY);
            return read(graph, graphElement);
        } catch (ParserConfigurationException | IOException | SAXException e) {
            throw new ReadException(e);
        }
    }

    private Graph<N, L> read(Graph<N, L> graph, Element graphElement) throws ReadException{
        try {
            Map<String, N> nodeIds = new HashMap<>();
            Element nodes = getElementByTagName(graphElement, NODES_ELEMENT_KEY);
            for (Element nodeElement : getElements(nodes)){
                String tagName = nodeElement.getTagName();
                NodeAdapter<? extends N> nodeAdapter = nodeAdaptersByTag.get(tagName);
                if (nodeAdapter == null) {
                    System.err.println("Could not create node of type \""+tagName+"\" because no appropriate factory was found");
                } else {
                    N node = nodeAdapter.createNode(DoubleTuples.ZEROS);
                    nodeIds.put(nodeElement.getAttribute(LINK_ID_ATTRIBUTE_KEY), node);
                    readFrom(nodeElement, storables(nodeAdapter, node));
                    graph.addNode(node);
                }
            }
            Element links = getElementByTagName(graphElement, LINKS_ELEMENT_KEY);
            for (Element linkElement : getElementsByTagName(links, LINK_ELEMENT_KEY)){
                try {
                    L link = readLink(linkElement, graph, nodeIds);
                    graph.establishLink(link);
                } catch (Graph.AlreadyLinkedException e) {
                    throw new ReadException("link established twice. first node: \""+
                            linkElement.getAttribute(FIRST_NODE_ID_ATTRIBUTE_KEY)+"\", second node: \""+
                            linkElement.getAttribute(SECOND_NODE_ID_ATTRIBUTE_KEY)+"\".");
                }
            }
            return graph;
        } catch (NumberFormatException e) {
            throw new ReadException(e);
        }
    }

    public Graph<N, L> readGraph(Element element) {
        Graph<N, L> graph = new Graph<>();
        readFrom(element, graphAdapter.getStorables(graph));
        return graph;
    }

    public double readScale(Element element) {
        return Double.valueOf(element.getAttribute(SCALE_ATTRIBUTE_KEY));
    }

    public L readLink(Element element, Graph<N, L> graph, Map<String, N> nodeIds){
        String firstId = readFirstNodeId(element);
        N node_first = nodeIds.get(firstId);
        if (node_first == null) {
            throw new RuntimeException("Link element referenced first node \""+firstId+"\" but that node doesn't exist.");
        }
        String secondId = readSecondNodeId(element);
        N node_second = nodeIds.get(secondId);
        if (node_second == null) {
            throw new RuntimeException("Link element referenced second node \""+secondId+"\" but that node doesn't exist.");
        }
        return linkFactory.createLink(node_first, node_second);
    }

    public String readFirstNodeId(Element element) {
        return element.getAttribute(FIRST_NODE_ID_ATTRIBUTE_KEY);
    }

    public String readSecondNodeId(Element element) {
        return element.getAttribute(SECOND_NODE_ID_ATTRIBUTE_KEY);
    }
}
