package at.plaz.graphs.persistence;

import at.plaz.graphs.Graph;
import at.plaz.graphs.Link;
import at.plaz.graphs.Node;

import java.io.File;

/**
 * Created by Georg Plaz.
 */
public interface GraphDAO<N extends Node, L extends Link<N>> extends FileDAO<Graph<N, L>>{
    Graph<N, L> read(File file) throws ReadException;
}
