package at.plaz.graphs.persistence;

import at.plaz.graphs.value.Storable;
import at.plaz.graphs.value.StorableLeaf;
import at.plaz.graphs.value.StorableNode;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public abstract class AbstractXMLDAO<A> implements FileDAO<A> {
    public static final String VALUE_ATTRIBUTE_KEY = "value";

    public Element appendTo(Element parentElement, Storable storable, Document document) {
        if (storable instanceof StorableLeaf) {
            return appendTo(parentElement, (StorableLeaf) storable, document);
        } else {
            return appendTo(parentElement, (StorableNode) storable, document);
        }
    }

    public Element appendTo(Element parentElement, StorableLeaf value, Document document) {
        Element element = createChildElement(value.key(), parentElement, document);
        if (!value.isNull()) {
            element.setAttribute(VALUE_ATTRIBUTE_KEY, value.getValueAsString());
        }
        return element;
    }

    public Element appendTo(Element parentElement, StorableNode node, Document document) {
        Element element = createChildElement(node.key(), parentElement, document);
        for (Storable storable : node.getStorables().values()) {
            appendTo(element, storable, document);
        }
        return element;
    }

    public void readFrom(Element element, StorableLeaf storableNode) {
        if (element.hasAttribute(VALUE_ATTRIBUTE_KEY)) {
            storableNode.setValueFromString(element.getAttribute(VALUE_ATTRIBUTE_KEY));
//            System.out.println("read value: \""+element.getAttribute(VALUE_ATTRIBUTE_KEY)+"\"");
        } else {
            System.err.println("Could not set value from xml element \""+element.getTagName()+"\" because the xml element didn't contain a \""+VALUE_ATTRIBUTE_KEY+"\" attribute!");
        }
    }

    public void readFrom(Element element, Storable storable) {
        if (storable instanceof StorableLeaf) {
            readFrom(element, (StorableLeaf) storable);
        } else {
            readFrom(element, (StorableNode) storable);
        }
    }

    public void readFrom(Element element, StorableNode storableNode) {
        for (String key : storableNode.getStorables().keySet()) {
            Storable storable = storableNode.getStorables().get(key);
            Element childElement = getElementByTagName(element, key);
            if (childElement != null) {
                readFrom(childElement, storable);
            } else {
                System.err.println("Could not set value \""+key+"\" because it wasn't set in the xml element!");
            }
        }
    }

    public Document createDocument() throws WriteException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        try {
            DocumentBuilder db = dbf.newDocumentBuilder();
            return db.newDocument();
        } catch (ParserConfigurationException e) {
            throw new WriteException(e);
        }
    }

    public Element createChildElement(String tagName, Element parent, Document document) {
        Element child = document.createElement(tagName);
        parent.appendChild(child);
        return child;
    }

    public Element getRootElement(File f) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        Document document = builder.parse(f);
        document.normalize();
        return document.getDocumentElement();
    }

    public List<Element> getElementsByTagName(Element root, String tag) {
        List<Element> toReturn = new LinkedList<>();
        NodeList nodeList = root.getElementsByTagName(tag);
        for (int i = 0; i < nodeList.getLength(); i++) {
            org.w3c.dom.Node node = nodeList.item(i);
            if (node instanceof Element) {
                toReturn.add((Element) node);
            }
        }
        return toReturn;
    }

    public List<Element> getElements(Element root) {
        List<Element> toReturn = new LinkedList<>();
        NodeList nodeList = root.getChildNodes();
        for (int i = 0; i < nodeList.getLength(); i++) {
            org.w3c.dom.Node node = nodeList.item(i);
            if (node instanceof Element) {
                toReturn.add((Element) node);
            }
        }
        return toReturn;
    }

    public Element getElementByTagName(Element root, String tag) {
        NodeList nodeList = root.getElementsByTagName(tag);
        if (nodeList.getLength() == 0) {
            return null;
        }
        return (Element) nodeList.item(0);
    }

    public void persistDocument(Document document, File file) throws WriteException {
        try {
            Transformer tr = TransformerFactory.newInstance().newTransformer();
            tr.setOutputProperty(OutputKeys.INDENT, "yes");
            tr.setOutputProperty(OutputKeys.METHOD, "xml");
            tr.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            tr.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            tr.transform(new DOMSource(document), new StreamResult(new FileOutputStream(file)));

        } catch (TransformerException | IOException e) {
            throw new WriteException(e);
        }
    }
}
