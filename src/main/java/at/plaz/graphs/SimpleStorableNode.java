package at.plaz.graphs;

import at.plaz.graphs.conversion.TwoWayConverter;
import at.plaz.graphs.value.*;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Georg Plaz.
 */
public class SimpleStorableNode implements StorableNode {
    private Map<String, Storable> storableMap = new LinkedHashMap<>();
    private String key;

    public SimpleStorableNode(String key) {
        this.key = key;
    }

    @Override
    public String key() {
        return key;
    }

    @Override
    public Map<String, Storable> getStorables() {
        return storableMap;
    }

    @Override
    public <A> void add(String key, Property<A> property, TwoWayConverter<String, A> converter) {
        SimpleStorable<A> storable = new SimpleStorable<>(key, property, converter);
        storableMap.put(key, storable);
    }
}
