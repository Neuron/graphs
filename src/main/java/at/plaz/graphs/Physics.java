package at.plaz.graphs;

/**
 * Created by Georg Plaz.
 */
public class Physics {
    private double repulsiveness = 1;
    private double factor = 1;

    public double getRepulsiveness() {
        return repulsiveness;
    }

    public void setRepulsiveness(double repulsiveness) {
        this.repulsiveness = repulsiveness;
    }

    public double getFactor() {
        return factor;
    }

    public void setFactor(double factor) {
        this.factor = factor;
    }
}
