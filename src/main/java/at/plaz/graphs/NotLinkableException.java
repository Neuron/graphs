package at.plaz.graphs;

/**
 * Created by Georg Plaz.
 */
public class NotLinkableException extends RuntimeException {
    private Node node;
    private Node secondNode;

    public NotLinkableException() {
    }

    public NotLinkableException(String message) {
        super(message);
    }

    public NotLinkableException(String message, Throwable cause) {
        super(message, cause);
    }

    public NotLinkableException(Throwable cause) {
        super(cause);
    }

    public NotLinkableException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }

    public NotLinkableException(Node node) {
        this.node = node;
    }

    public NotLinkableException(String message, Node node) {
        super(message);
        this.node = node;
    }

    public NotLinkableException(String message, Throwable cause, Node node) {
        super(message, cause);
        this.node = node;
    }

    public NotLinkableException(Throwable cause, Node node) {
        super(cause);
        this.node = node;
    }

    public NotLinkableException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, Node node) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.node = node;
    }


    public NotLinkableException(Node node, Node secondNode) {
        this.node = node;
        this.secondNode = secondNode;
    }

    public NotLinkableException(String message, Node node, Node secondNode) {
        super(message);
        this.node = node;
        this.secondNode = secondNode;
    }

    public NotLinkableException(String message, Throwable cause, Node node, Node secondNode) {
        super(message, cause);
        this.node = node;
        this.secondNode = secondNode;
    }

    public NotLinkableException(Throwable cause, Node node, Node secondNode) {
        super(cause);
        this.node = node;
        this.secondNode = secondNode;
    }

    public NotLinkableException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace, Node node, Node secondNode) {
        super(message, cause, enableSuppression, writableStackTrace);
        this.node = node;
        this.secondNode = secondNode;
    }

    public Node getNode() {
        return node;
    }

    public Node getSecondNode() {
        return secondNode;
    }
}
