package at.plaz.graphs;

import at.plaz.graphs.persistence.DAOAdapter;
import at.plaz.graphs.value.StorableNode;

/**
 * Created by Georg Plaz.
 */
public interface NodeAdapter<N extends Node> extends DAOAdapter<N>, NodeFactory<N> {
    String getKey();

    Class<N> getAdapterClass();

    StorableNode getStorables(N node);
}
