package at.plaz.graphs;

import at.plaz.graphs.forces.CircularContainerForce;
import at.plaz.graphs.forces.Location;

/**
 * Created by Georg Plaz.
 */
public class Main {
    public static void main(String... args) {
        Graph<SimpleNode, SimpleLink<SimpleNode>> graph = new Graph<>();
        graph.equilibriumDistance().set(100);
        graph.intensityScale().set(0.1);
        SimpleNode firstNode = new SimpleNode(0, 0);
        firstNode.fixed().set(false);
        graph.addNode(firstNode);
        SimpleNode secondNode = new SimpleNode(200, 0);
        secondNode.fixed().set(false);
        graph.addNode(secondNode);
        graph.establishLink(new SimpleLink<>(firstNode, secondNode));
        SimpleNode lastNode = secondNode;

        graph.addForce(new CircularContainerForce<>(new Location(0, 0), 50, 1));

//        for (int i = 2; i < 10; i++) {
//
//            SimpleNode nextNode = new SimpleNode(100*i, 0);
//            graph.addNode(nextNode);
//            graph.establishLink(new SimpleLink<>(lastNode, nextNode));
//            lastNode = nextNode;
//        }
        System.out.println(firstNode.location());
        for (int i = 0; i < 50; i++) {
            System.out.println("\n(x, y)_0: "+firstNode.location().get());
            System.out.println("(x, y)_1: "+secondNode.location().get());
            System.out.println("distance: "+firstNode.location().get().distance(secondNode.location().get()));
            for (int j = 0; j < 5; j++) {
                graph.update();
            }
        }
    }
}
