package at.plaz.graphs;

import at.plaz.tuples.SymTupleF;

/**
 * Created by Georg Plaz.
 */
public interface Link<N extends Node> extends SymTupleF<N> {

}
