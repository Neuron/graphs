package at.plaz.graphs;

/**
 * Created by Georg Plaz.
 */
public interface LinkRemovedListener<N extends Node, L extends Link<N>> {
    void removed(L link);
}
