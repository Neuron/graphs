package at.plaz.graphs;

import at.plaz.graphs.value.StorableNode;
import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public abstract class AbstractNodeAdapter<N extends Node> implements NodeAdapter<N> {
    private String nodeKey;
    private Class<N> nodeClass;
    private NodeFactory<N> nodeFactory;

    public AbstractNodeAdapter(String nodeKey, Class<N> nodeClass, NodeFactory<N> nodeFactory) {
        this.nodeKey = nodeKey;
        this.nodeClass = nodeClass;
        this.nodeFactory = nodeFactory;
    }

    @Override
    public String getKey() {
        return nodeKey;
    }

    public void setNodeKey(String nodeKey) {
        this.nodeKey = nodeKey;
    }

    @Override
    public Class<N> getAdapterClass() {
        return nodeClass;
    }

    public void setNodeClass(Class<N> nodeClass) {
        this.nodeClass = nodeClass;
    }

    public NodeFactory<N> getNodeFactory() {
        return nodeFactory;
    }

    public void setNodeFactory(NodeFactory<N> nodeFactory) {
        this.nodeFactory = nodeFactory;
    }

    @Override
    public N createNode(DoubleTuple center) {
        return nodeFactory.createNode(center);
    }

    public StorableNode getStorables(N node) {
        StorableNode storableNode = new SimpleStorableNode(nodeKey);
        addStorables(node, storableNode);
        return storableNode;
    }

    public abstract void addStorables(N node, StorableNode storableNode);
}
