package at.plaz.graphs.value;

import at.plaz.tuples.DefaultDoubleTupleF;
import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public interface DoubleValue extends Value<Double> {
    default double value() {
        return get();
    }

    default DoubleValue multiply(double factor) {
        return new DoubleValueModifier(DoubleValue.this) {
            @Override
            public double modify(double toModify) {
                return toModify * factor;
            }
        };
    }

    default DoubleValue multiply(DoubleValue other) {
        return new TwoDoubleValueModifier(DoubleValue.this, other) {
            @Override
            public double modify(double val1, double val2) {
                return val1 * val2;
            }
        };
    }

    default DoubleValue add(double d) {
        return new DoubleValueModifier(DoubleValue.this) {
            @Override
            public double modify(double toModify) {
                return toModify + d;
            }
        };
    }

    default DoubleValue add(DoubleValue other) {
        return new TwoDoubleValueModifier(DoubleValue.this, other) {
            @Override
            public double modify(double val1, double val2) {
                return val1 + val2;
            }
        };
    }

    default DoubleTupleValue toDoubleTuple() {
        return new DoubleToDoubleTupleModifier(DoubleValue.this){

            @Override
            public DoubleTuple modify(Double value) {
                return new DefaultDoubleTupleF(value, value);
            }
        };
    }
}
