package at.plaz.graphs.value;

import at.plaz.graphs.Graph;
import at.plaz.graphs.Link;
import at.plaz.graphs.Node;
import at.plaz.graphs.SimpleStorableNode;
import at.plaz.graphs.persistence.DAOAdapter;

/**
 * Created by Georg Plaz.
 */
public class GraphAdapter<N extends Node, L extends Link<N>> implements DAOAdapter<Graph<N, L>> {
    private String key;
    private Class<Graph<N, L>> graphClass;

    public GraphAdapter(String key, Class graphClass) {
        this.key = key;
        this.graphClass = graphClass;
    }

    @Override
    public String getKey() {
        return key;
    }

    @Override
    public Class<Graph<N, L>> getAdapterClass() {
        return graphClass;
    }

    @Override
    public StorableNode getStorables(Graph<N, L> node) {
        SimpleStorableNode storableNode = new SimpleStorableNode(key);
        storableNode.addDouble("force_equilibrium", node.equilibriumDistance());
        storableNode.addDouble("force_intensity", node.intensityScale());
        storableNode.addDouble("friction", node.friction());
        return storableNode;
    }
}
