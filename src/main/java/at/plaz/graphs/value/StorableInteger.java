package at.plaz.graphs.value;

import at.plaz.graphs.conversion.IntegerConverter;

/**
 * Created by Georg Plaz.
 */
public class StorableInteger extends SimpleStorableProperty<Integer> {
    public StorableInteger(String key) {
        super(key, IntegerConverter.instance());
    }

    public StorableInteger(int value, String key) {
        super(value, key, IntegerConverter.instance());
    }
}
