package at.plaz.graphs.value;

import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public abstract class DoubleTupleModifier extends ValueModifier<DoubleTuple, DoubleTuple> implements DoubleTupleValue {
    protected DoubleTupleModifier(DoubleTupleValue base) {
        super(base);
    }
}
