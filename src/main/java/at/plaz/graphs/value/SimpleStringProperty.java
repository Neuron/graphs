package at.plaz.graphs.value;


/**
 * Created by Georg Plaz.
 */
public class SimpleStringProperty extends SimpleProperty<String> implements StringProperty {
    public SimpleStringProperty() {
    }

    public SimpleStringProperty(String value) {
        super(value);
    }

    @Override
    public String toString() {
        return "StringProperty{"+get()+"}";
    }
}
