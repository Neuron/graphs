package at.plaz.graphs.value;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Georg Plaz.
 */
public abstract class BooleanValueModifier implements BooleanValue {
    private BooleanValue base;
    private Map<ValueChangedListener, ValueChangedListener> listeners = new HashMap<>();

    protected BooleanValueModifier(BooleanValue base) {
        this.base = base;
    }

    @Override
    public Boolean get() {
        return modify(base.get());
    }

    @Override
    public boolean value() {
        return modify(base.value());
    }

    @Override
    public void addValueChangedListener(ValueChangedListener<? super Boolean> valueChangedListener) {
        ValueChangedListener<Boolean> wrappedListener = (oldValue, newValue) -> valueChangedListener.changed(boxedModify(oldValue), boxedModify(newValue));
        listeners.put(valueChangedListener, wrappedListener);
        base.addValueChangedListener(wrappedListener);
    }

    @Override
    public void removeValueChangedListener(ValueChangedListener<? super Boolean> valueChangedListener) {
        base.removeValueChangedListener(listeners.get(valueChangedListener));
        listeners.remove(valueChangedListener);
    }

    private Boolean boxedModify(Boolean toModify) {
        if (toModify == null) {
            return null;
        }
        return modify(toModify);
    }

    public abstract boolean modify(boolean toModify);
}
