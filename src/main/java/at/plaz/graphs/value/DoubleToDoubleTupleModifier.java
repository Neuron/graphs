package at.plaz.graphs.value;

import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public abstract class DoubleToDoubleTupleModifier extends ValueModifier<Double, DoubleTuple> implements DoubleTupleValue {
    protected DoubleToDoubleTupleModifier(Value<Double> base) {
        super(base);
    }
}
