package at.plaz.graphs.value;

/**
 * Created by Georg Plaz.
 */
public interface StringProperty extends StringValue, Property<String> {
}
