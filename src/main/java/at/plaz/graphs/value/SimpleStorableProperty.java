package at.plaz.graphs.value;

import at.plaz.graphs.conversion.TwoWayConverter;

/**
 * Created by Georg Plaz.
 */
public class SimpleStorableProperty<A> extends SimpleProperty<A> implements StorableProperty<A> {
    private String key;
    private Property<A> property;
    private TwoWayConverter<String, A> converter;

    public SimpleStorableProperty(String key, TwoWayConverter<String, A> converter) {
        this.key = key;
        this.converter = converter;
    }

    public SimpleStorableProperty(A value, String key, TwoWayConverter<String, A> converter) {
        super(value);
        this.key = key;
        this.converter = converter;
    }

    @Override
    public String key() {
        return key;
    }

    @Override
    public String getValueAsString() {
        return converter.convertBack(get());
    }

    @Override
    public void setValueFromString(String valueAsString) {
        set(converter.convert(valueAsString));
    }
}
