package at.plaz.graphs.value;

import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public abstract class DoubleDoubleToDoubleTupleModifier extends TwoValueModifier<Double, Double, DoubleTuple> implements DoubleTupleValue {
    protected DoubleDoubleToDoubleTupleModifier(Value<Double> base, Value<Double> base2) {
        super(base, base2);
    }
}
