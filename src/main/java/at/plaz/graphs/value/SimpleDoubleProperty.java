package at.plaz.graphs.value;


/**
 * Created by Georg Plaz.
 */
public class SimpleDoubleProperty extends SimpleProperty<Double> implements DoubleProperty {
    public SimpleDoubleProperty() {
    }

    public SimpleDoubleProperty(double value) {
        super(value);
    }

    @Override
    public String toString() {
        return "DoubleProperty{"+get()+"}";
    }
}
