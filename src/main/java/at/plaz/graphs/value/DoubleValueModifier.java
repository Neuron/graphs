package at.plaz.graphs.value;

/**
 * Created by Georg Plaz.
 */
public abstract class DoubleValueModifier extends ValueModifier<Double, Double> implements DoubleValue {
//    private Map<ValueChangedListener, ValueChangedListener> listeners = new HashMap<>();
    private DoubleValue base;

    protected DoubleValueModifier(DoubleValue base) {
        super(base);
        this.base = base;
    }

    @Override
    public Double get() {
        return modify(base.get());
    }

    @Override
    public double value() {
        return modify(base.value());
    }

    @Override
    public Double modify(Double value) {
        return modify(value.doubleValue());
    }


    public abstract double modify(double toModify);
}
