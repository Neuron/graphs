package at.plaz.graphs.value;

import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public abstract class TwoDoubleTupleModifier extends TwoValueModifier<DoubleTuple, DoubleTuple, DoubleTuple> implements DoubleTupleValue {
    protected TwoDoubleTupleModifier(Value<DoubleTuple> base, Value<DoubleTuple> base2) {
        super(base, base2);
    }
}
