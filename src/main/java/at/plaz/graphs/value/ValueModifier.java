package at.plaz.graphs.value;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Georg Plaz.
 */
public abstract class ValueModifier<A, B> implements Value<B> {
    private Value<A> base;
    private Map<ValueChangedListener, ValueChangedListener> listeners = new HashMap<>();

    public Value<A> getBase() {
        return base;
    }

    protected ValueModifier(Value<A> base) {
        this.base = base;
    }

    @Override
    public B get() {
        return modify(base.get());
    }

    @Override
    public void addValueChangedListener(ValueChangedListener<? super B> valueChangedListener) {
        ValueChangedListener<A> listener = (oldValue, newValue) -> valueChangedListener.changed(modify(oldValue), modify(newValue));
        base.addValueChangedListener(listener);
        listeners.put(valueChangedListener, listener);
    }

    @Override
    public void removeValueChangedListener(ValueChangedListener<? super B> valueChangedListener) {
        base.removeValueChangedListener(listeners.get(valueChangedListener));
        listeners.remove(valueChangedListener);
    }
    
    public abstract B modify(A value);
}
