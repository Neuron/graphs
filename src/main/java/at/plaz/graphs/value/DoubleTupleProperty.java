package at.plaz.graphs.value;

import at.plaz.tuples.DefaultDoubleTupleF;
import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public interface DoubleTupleProperty extends DoubleTupleValue, Property<DoubleTuple> {
    default void set(double first, double second) {
        set(new DefaultDoubleTupleF(first, second));
    }
}
