package at.plaz.graphs.value;

import at.plaz.graphs.conversion.DoubleConverter;

/**
 * Created by Georg Plaz.
 */
public class StorableDouble extends SimpleStorableProperty<Double> {
    public StorableDouble(String key) {
        super(key, DoubleConverter.instance());
    }

    public StorableDouble(double value, String key) {
        super(value, key, DoubleConverter.instance());
    }
}
