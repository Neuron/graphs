package at.plaz.graphs.value;

import at.plaz.graphs.conversion.*;
import at.plaz.tuples.DoubleTuple;

import java.io.File;
import java.util.Collections;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Created by Georg Plaz.
 */
public interface StorableNode extends Storable{
    default Map<String, Storable> getStorables() {return Collections.emptyMap();}
    default boolean hasStorables() {return getStorables().size() > 0;}

    static Map<String, Storable> mapStorables(Storable... leaves){
        return mapStorables(new LinkedHashMap<>(), leaves);
    }

    static  Map<String, Storable> mapStorables(Map<String, Storable> storables, Storable... leaves){
        for (Storable storable : leaves) {
            storables.put(storable.key(), storable);
        }
        return storables;
    }

    <A> void add(String key, Property<A> property, TwoWayConverter<String, A> converter);

    default void addBoolean(String key, Property<Boolean> property) {
        add(key, property, BooleanConverter.instance());
    }

    default void addString(String key, Property<String> property) {
        add(key, property, IdentityConverter.instance());
    }

    default void addInteger(String key, Property<Integer> property) {
        add(key, property, IntegerConverter.instance());
    }

    default void addDouble(String key, Property<Double> property) {
        add(key, property, DoubleConverter.instance());
    }

    default void addDoubleTuple(String key, Property<DoubleTuple> property) {
        add(key, property, DoubleTupleConverter.instance());
    }

    default void addFile(String key, Property<File> property) {
        add(key, property, FileConverter.instance());
    }
}
