package at.plaz.graphs.value;

import at.plaz.tuples.DefaultSymTupleF;
import at.plaz.tuples.SymTuple;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Georg Plaz.
 */
public abstract class TwoValueModifier<A, B, C> implements Value<C> {
    private Value<A> base;
    private Value<B> base2;
    private Map<ValueChangedListener, SymTuple<ValueChangedListener>> listeners = new HashMap<>();

    protected TwoValueModifier(Value<A> base, Value<B> base2) {
        this.base = base;
        this.base2 = base2;
    }

    @Override
    public C get() {
        return modify(base.get(), base2.get());
    }

    @Override
    public void addValueChangedListener(ValueChangedListener<? super C> valueChangedListener) {
        ValueChangedListener<A> firstListener = (oldValue, newValue) -> valueChangedListener.changed(modify(oldValue, base2.get()), modify(newValue, base2.get()));
        ValueChangedListener<B> secondListener = (oldValue, newValue) -> valueChangedListener.changed(modify(base.get(), oldValue), modify(base.get(), newValue));
        base.addValueChangedListener(firstListener);
        base2.addValueChangedListener(secondListener);
        listeners.put(valueChangedListener, new DefaultSymTupleF<>(firstListener, secondListener));
    }

    @Override
    public void removeValueChangedListener(ValueChangedListener<? super C> valueChangedListener) {
        base.removeValueChangedListener(listeners.get(valueChangedListener).getFirst());
        base2.removeValueChangedListener(listeners.get(valueChangedListener).getSecond());
        listeners.remove(valueChangedListener);
    }
    
    public abstract C modify(A val1, B val2);
}
