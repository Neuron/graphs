package at.plaz.graphs.value;

import at.plaz.tuples.DefaultDoubleTupleF;
import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public class SimpleDoubleTupleProperty extends SimpleProperty<DoubleTuple> implements DoubleTupleProperty {
    public SimpleDoubleTupleProperty() {
    }

    public SimpleDoubleTupleProperty(DoubleTuple value) {
        super(value);
    }

    public SimpleDoubleTupleProperty(double x, double y) {
        super(new DefaultDoubleTupleF(x, y));
    }

    @Override
    public String toString() {
        return "DoubleTupleProperty{"+get()+"}";
    }
}
