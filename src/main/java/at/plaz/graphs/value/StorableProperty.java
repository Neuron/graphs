package at.plaz.graphs.value;

/**
 * Created by Georg Plaz.
 */
public interface StorableProperty<A> extends StorableValue<A>, Property<A> {
    @Override
    default boolean isNull() {
        return get() != null;
    }
}
