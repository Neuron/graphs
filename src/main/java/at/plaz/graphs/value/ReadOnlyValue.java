package at.plaz.graphs.value;

/**
 * Created by Georg Plaz.
 */
public interface ReadOnlyValue<A> extends Value<A> {
}
