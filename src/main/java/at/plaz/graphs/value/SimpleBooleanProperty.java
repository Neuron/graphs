package at.plaz.graphs.value;


/**
 * Created by Georg Plaz.
 */
public class SimpleBooleanProperty extends SimpleProperty<Boolean> implements BooleanProperty {
    public SimpleBooleanProperty() {
    }

    public SimpleBooleanProperty(Boolean value) {
        super(value);
    }

    @Override
    public String toString() {
        return "BooleanProperty{"+get()+"}";
    }
}
