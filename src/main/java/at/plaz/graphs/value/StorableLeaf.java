package at.plaz.graphs.value;

/**
 * Created by Georg Plaz.
 */
public interface StorableLeaf extends Storable{
    String key();
    boolean isNull();
    String getValueAsString();
    void setValueFromString(String valueAsString);
}
