package at.plaz.graphs.value;

/**
 * Created by Georg Plaz.
 */
public interface BooleanValue extends Value<Boolean> {
    default boolean value() {return get();}

    default BooleanValue flip() {
        return new BooleanValueModifier(BooleanValue.this) {
            @Override
            public boolean modify(boolean toModify) {
                return !toModify;
            }
        };
    }
}
