package at.plaz.graphs.value;

/**
 * Created by Georg Plaz.
 */
public interface Property<A> extends Value<A> {
    void set(A newValue);

    void bind(Value<A> value);

    void unbind();

    boolean isBound();
}
