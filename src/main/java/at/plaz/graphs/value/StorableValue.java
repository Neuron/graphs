package at.plaz.graphs.value;

/**
 * Created by Georg Plaz.
 */
public interface StorableValue<A> extends Value<A>, StorableLeaf{
}
