package at.plaz.graphs.value;

import at.plaz.tuples.DefaultDoubleTupleF;
import at.plaz.tuples.DoubleTuple;
import at.plaz.tuples.DoubleTuples;

/**
 * Created by Georg Plaz.
 */
public interface DoubleTupleValue extends Value<DoubleTuple> {

    default DoubleTupleValue multiply(double factor) {
        return new DoubleTupleModifier(DoubleTupleValue.this) {
            @Override
            public DoubleTuple modify(DoubleTuple value) {
                return DoubleTuples.multiply(value, factor);
            }
        };
    }

    default DoubleTupleValue multiply(DoubleTupleValue other) {
        return new TwoDoubleTupleModifier(DoubleTupleValue.this, other) {
            @Override
            public DoubleTuple modify(DoubleTuple val1, DoubleTuple val2) {
                return DoubleTuples.multiply(val1, val2);
            }
        };
    }

    default DoubleTupleValue add(double d) {
        return new DoubleTupleModifier(DoubleTupleValue.this) {

            @Override
            public DoubleTuple modify(DoubleTuple value) {
                return DoubleTuples.add(value, d);
            }
        };
    }

    default DoubleTupleValue add(DoubleTupleValue other) {
        return new TwoDoubleTupleModifier(DoubleTupleValue.this, other) {
            @Override
            public DoubleTuple modify(DoubleTuple val1, DoubleTuple val2) {
                return DoubleTuples.add(val1, val2);
            }
        };
    }

    default DoubleValue getFirst() {
        return new DoubleTupleToDoubleModifier(DoubleTupleValue.this) {
            @Override
            public Double modify(DoubleTuple value) {
                return value.first();
            }
        };
    }

    default DoubleValue getSecond() {
        return new DoubleTupleToDoubleModifier(DoubleTupleValue.this) {
            @Override
            public Double modify(DoubleTuple value) {
                return value.second();
            }
        };
    }

    static DoubleTupleValue create(DoubleValue first, DoubleValue second) {
        return new DoubleDoubleToDoubleTupleModifier(first, second) {
            @Override
            public DoubleTuple modify(Double val1, Double val2) {
                return new DefaultDoubleTupleF(val1, val2);
            }
        };
    }

    static DoubleTupleValue create(double first, DoubleValue second) {
        return new DoubleToDoubleTupleModifier(second) {
            @Override
            public DoubleTuple modify(Double value) {
                return new DefaultDoubleTupleF(first, value);
            }
        };
    }

    static DoubleTupleValue create(DoubleValue first, double second) {
        return new DoubleToDoubleTupleModifier(first) {
            @Override
            public DoubleTuple modify(Double value) {
                return new DefaultDoubleTupleF(value, second);
            }
        };
    }
}
