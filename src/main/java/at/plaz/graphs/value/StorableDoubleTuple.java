package at.plaz.graphs.value;

import at.plaz.graphs.conversion.DoubleTupleConverter;
import at.plaz.tuples.DefaultDoubleTupleF;
import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public class StorableDoubleTuple extends SimpleStorableProperty<DoubleTuple> {
    public StorableDoubleTuple(String key) {
        super(key, DoubleTupleConverter.instance());
    }

    public StorableDoubleTuple(DoubleTuple value, String key) {
        super(value, key, DoubleTupleConverter.instance());
    }

    public StorableDoubleTuple(double x, double y, String key) {
        super(new DefaultDoubleTupleF(x, y), key, DoubleTupleConverter.instance());
    }
}
