package at.plaz.graphs.value;

/**
 * Created by Georg Plaz.
 */
public interface StringValue extends Value<String> {

}
