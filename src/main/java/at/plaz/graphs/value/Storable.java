package at.plaz.graphs.value;

/**
 * Created by Georg Plaz.
 */
public interface Storable {
    String key();
}
