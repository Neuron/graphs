package at.plaz.graphs.value;

import at.plaz.graphs.conversion.IdentityConverter;

/**
 * Created by Georg Plaz.
 */
public class StorableString extends SimpleStorableProperty<String> {
    public StorableString(String key) {
        super(key, IdentityConverter.instance());
    }

    public StorableString(String value, String key) {
        super(value, key, IdentityConverter.instance());
    }
}
