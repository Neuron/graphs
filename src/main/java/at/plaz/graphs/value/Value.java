package at.plaz.graphs.value;

/**
 * Created by Georg Plaz.
 */
public interface Value<A> {
    A get();
    default boolean isNull() {return get() == null;}
    void addValueChangedListener(ValueChangedListener<? super A> valueChangedListener);
    void removeValueChangedListener(ValueChangedListener<? super A> valueChangedListener);
}
