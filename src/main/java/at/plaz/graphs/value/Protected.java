package at.plaz.graphs.value;

/**
 * Created by Georg Plaz.
 */
public class Protected<A> implements Value<A> {
    private Value<? extends A> val;

    @Override
    public boolean isNull() {
        return val.isNull();
    }

    public Protected(Value<? extends A> val) {
        this.val = val;
    }

    @Override
    public A get() {
        return val.get();
    }

    @Override
    public void addValueChangedListener(ValueChangedListener<? super A> valueChangedListener) {
        val.addValueChangedListener(valueChangedListener);
    }

    @Override
    public void removeValueChangedListener(ValueChangedListener<? super A> valueChangedListener) {
        val.removeValueChangedListener(valueChangedListener);
    }
}
