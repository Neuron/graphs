package at.plaz.graphs.value;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Georg Plaz.
 */
public class SimpleProperty<A> implements Property<A> {
    private Value<A> bound;
    private ValueChangedListener<A> boundListener;
    private List<ValueChangedListener<? super A>> valueChangedListeners = null;
    private A value;

    public SimpleProperty() {

    }

    public SimpleProperty(A value) {
        this.value = value;
    }

    @Override
    public A get() {
        return value;
    }

    @Override
    public void set(A value) {
        if (bound != null) {
            throw new RuntimeException("Can't set bound property!");
        }
        privateSet(value);
    }

    private void privateSet(A value) {
        A oldValue = this.value;
        if (oldValue == null || !oldValue.equals(value)){
            this.value = value;
            if (valueChangedListeners != null) {
                for (ValueChangedListener<? super A> valueChangedListener : valueChangedListeners) {
                    valueChangedListener.changed(oldValue, value);
                }
            }
        }
    }

    @Override
    public void addValueChangedListener(ValueChangedListener<? super A> valueChangedListener) {
        if (valueChangedListeners == null) {
            valueChangedListeners = new LinkedList<>();
        }
        valueChangedListeners.add(valueChangedListener);
    }

    @Override
    public void removeValueChangedListener(ValueChangedListener<? super A> valueChangedListener) {
        if (valueChangedListeners != null) {
            valueChangedListeners.remove(valueChangedListener);
            if (valueChangedListeners.isEmpty()) {
                valueChangedListeners = null;
            }
        }
    }

    @Override
    public void bind(Value<A> value) {
        bound = value;
        value.addValueChangedListener(boundListener = (oldValue, newValue) -> privateSet(newValue));
        privateSet(value.get());
    }

    @Override
    public void unbind() {
        bound.removeValueChangedListener(boundListener);
        bound = null;
        boundListener = null;
    }

    @Override
    public boolean isBound() {
        return bound != null;
    }

    @Override
    public String toString() {
        return "SimpleProperty{" +
                "value=" + value +
                '}';
    }
}
