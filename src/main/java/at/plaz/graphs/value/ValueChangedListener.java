package at.plaz.graphs.value;

/**
 * Created by Georg Plaz.
 */
public interface ValueChangedListener<A> {
    void changed(A oldValue, A newValue);
}
