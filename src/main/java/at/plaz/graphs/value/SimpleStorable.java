package at.plaz.graphs.value;

import at.plaz.graphs.conversion.TwoWayConverter;

/**
 * Created by Georg Plaz.
 */
public class SimpleStorable<A> implements StorableLeaf {
    private String key;
    private Property<A> property;
    private TwoWayConverter<String, A> converter;

    public SimpleStorable(String key, Property<A> property, TwoWayConverter<String, A> converter) {
        this.key = key;
        this.property = property;
        this.converter = converter;
    }

    @Override
    public String key() {
        return key;
    }

    @Override
    public boolean isNull() {
        return property.isNull();
    }

    @Override
    public String getValueAsString() {
        return converter.convertBack(property.get());
    }

    @Override
    public void setValueFromString(String valueAsString) {
        property.set(converter.convert(valueAsString));
    }
}
