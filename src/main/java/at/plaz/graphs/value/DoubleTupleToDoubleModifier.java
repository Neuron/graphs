package at.plaz.graphs.value;

import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public abstract class DoubleTupleToDoubleModifier extends ValueModifier<DoubleTuple,Double> implements DoubleValue {
    protected DoubleTupleToDoubleModifier(Value<DoubleTuple> base) {
        super(base);
    }
}
