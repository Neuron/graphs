package at.plaz.graphs.value;

import at.plaz.tuples.DefaultSymTupleF;
import at.plaz.tuples.SymTuple;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by Georg Plaz.
 */
public abstract class TwoDoubleValueModifier implements DoubleValue {
    private DoubleValue base;
    private DoubleValue base2;
    private Map<ValueChangedListener, SymTuple<ValueChangedListener>> listeners = new HashMap<>();

    protected TwoDoubleValueModifier(DoubleValue base, DoubleValue base2) {
        this.base = base;
        this.base2 = base2;
    }

    @Override
    public Double get() {
        return modify(base.get(), base2.get());
    }

    @Override
    public double value() {
        return modify(base.value(), base2.value());
    }

    @Override
    public void addValueChangedListener(ValueChangedListener<? super Double> valueChangedListener) {
        ValueChangedListener<Double> firstListener = (oldValue, newValue) -> valueChangedListener.changed(boxedModify(oldValue, base2.get()), modify(newValue, base2.get()));
        ValueChangedListener<Double> secondListener = (oldValue, newValue) -> valueChangedListener.changed(boxedModify(base.get(), oldValue), modify(base.get(), newValue));
        base.addValueChangedListener(firstListener);
        base2.addValueChangedListener(secondListener);
        listeners.put(valueChangedListener, new DefaultSymTupleF<>(firstListener, secondListener));
    }

    @Override
    public void removeValueChangedListener(ValueChangedListener<? super Double> valueChangedListener) {
        base.removeValueChangedListener(listeners.get(valueChangedListener).getFirst());
        base2.removeValueChangedListener(listeners.get(valueChangedListener).getSecond());
        listeners.remove(valueChangedListener);
    }
    
    private Double boxedModify(Double v1, Double v2) {
        if (v1 == null || v2 == null) {
            return null;
        }
        return modify(v1, v2);
    }
    
    public abstract double modify(double val1, double val2);
}
