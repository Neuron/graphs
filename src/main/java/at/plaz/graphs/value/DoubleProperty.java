package at.plaz.graphs.value;

/**
 * Created by Georg Plaz.
 */
public interface DoubleProperty extends DoubleValue, Property<Double> {
    default void set(double d) {
        set(Double.valueOf(d));
    }
}
