package at.plaz.graphs.value;

/**
 * Created by Georg Plaz.
 */
public interface BooleanProperty extends BooleanValue, Property<Boolean> {
}
