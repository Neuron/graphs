package at.plaz.graphs;

import at.plaz.graphs.persistence.GraphXMLDAO;

/**
 * Created by Georg Plaz.
 */
public class AbstractGraphXMLDAO extends GraphXMLDAO<Node, Link<Node>> {
    public AbstractGraphXMLDAO() {
        super(SimpleLink::new);
        addNodeAdapter(new SimpleNodeAdapter());
    }
}
