package at.plaz.graphs;

/**
 * Created by Georg Plaz.
 */
public interface NodeRemovedListener<N extends Node> {
    void removed(N node);
}
