package at.plaz.graphs;

import at.plaz.tuples.DoubleTuple;
import at.plaz.tuples.DoubleTupleF;
import at.plaz.tuples.DoubleTuples;

import static at.plaz.tuples.DoubleTuples.*;

/**
 * Created by Georg Plaz.
 */
public class DragAndDrop<A extends Draggable> {
    private A draggable;
    private DoubleTupleF lastPosition;
    private DoubleTupleF lastDelta;

    public void startDrag(A target, DoubleTuple mouseCenter) {
        if (isDragging()) {
            stopDrag();
        }
        this.draggable = target;
        lastPosition = mouseCenter.toFinal();
        lastDelta = DoubleTuples.ZEROS;
        target.startDrag();
    }

    public DoubleTuple update(DoubleTuple mouseCenter) {
        if (isDragging()) {
            lastDelta = subtract(mouseCenter, lastPosition);
            draggable.move(lastDelta);
            lastPosition = mouseCenter.toFinal();
            return lastDelta;
        } else {
            return null;
        }
    }

    public void stopDrag() {
        if (isDragging()) {
            draggable.stopDrag();
            draggable = null;
            lastPosition = null;
        }
    }

    public A getTarget() {
        return draggable;
    }

    public boolean isDragging() {
        return draggable != null;
    }

    public DoubleTupleF getLastDelta() {
        return lastDelta;
    }
}
