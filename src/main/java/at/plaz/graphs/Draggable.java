package at.plaz.graphs;

import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public interface Draggable {
    default void startDrag() {

    }

    default void stopDrag() {
        
    }

    void move(DoubleTuple delta);
}
