package at.plaz.graphs;

import at.plaz.graphs.value.*;
import at.plaz.tuples.DefaultDoubleTupleF;
import at.plaz.tuples.DoubleTuple;
import at.plaz.tuples.DoubleTupleF;
import at.plaz.tuples.DoubleTuples;

import static at.plaz.tuples.DoubleTuples.*;

/**
 * Created by Georg Plaz.
 */
public class SimpleNode implements Node {
    private final DoubleTupleProperty location;
    private final DoubleTupleProperty speed;
    private final BooleanProperty fixed;
    private final BooleanProperty linkable;
    private Property<Force<?>> force;
    private final Property<Graph> graph;
    private ValueChangedListener<Graph> addDefaultForceListener;

    public SimpleNode(DoubleTupleF location) {
        this.location = new SimpleDoubleTupleProperty(location);
        speed = new SimpleDoubleTupleProperty(DoubleTuples.ZEROS);
        fixed = new SimpleBooleanProperty(true);
        linkable = new SimpleBooleanProperty(true);
        force = new SimpleProperty<>();
        graph = new SimpleProperty<>();
        force.addValueChangedListener((oldValue, newValue) -> {
            if (!graph.isNull()) {
                if (oldValue != null) {
                    graph.get().removeForce(oldValue);
                }
                if (newValue != null) {
                    graph.get().addForce(newValue);
                }
            }
        });
        graph.addValueChangedListener(((oldValue, newValue) -> {
            if (!force.isNull()) {
                if (oldValue != null) {
                    oldValue.removeForce(force.get());
                }
                if (newValue != null) {
                    newValue.addForce(force.get());
                }
            }
        }));
        graph.addValueChangedListener(addDefaultForceListener = (oldValue, newValue) -> force.set(createDefaultForce(newValue)));
    }

    public Property<Force<?>> force() {
        return force;
    }

    public SimpleNode(double x, double y) {
        this(new DefaultDoubleTupleF(x, y));
    }

    public SimpleNode(DoubleTuple location) {
        this(location.toFinal());
    }

    @Override
    public DoubleTupleProperty location() {
        return location;
    }

    @Override
    public DoubleTupleProperty speed() {
        return speed;
    }

    @Override
    public void move(DoubleTuple delta) {
        location.set(add(location.get(), delta));
    }

//    @Override
//    public void setCenter(DoubleTupleF location) {
//        this.location = location;
//    }

    @Override
    public BooleanProperty fixed() {
        return fixed;
    }

    public Force<?> createDefaultForce(Graph<?, ?> graph) {
        return graph.createEquilibriumForce(this);
    }

    @Override
    public Property<Graph> graph() {
        return graph;
    }

    @Override
    public BooleanProperty linkable() {
        return linkable;
    }
}
