package at.plaz.graphs.conversion;

/**
 * Created by Georg Plaz.
 */
public interface TwoWayConverter<A, B> extends Converter<A, B> {
    B convert(A value);
    A convertBack(B value);
}
