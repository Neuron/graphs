package at.plaz.graphs.conversion;

/**
 * Created by Georg Plaz.
 */
public class DoubleConverter implements TwoWayConverter<String, Double> {
    private static final DoubleConverter instance = new DoubleConverter();
    private DoubleConverter() {

    }

    public static DoubleConverter instance() {
        return instance;
    }

    @Override
    public Double convert(String value) {
        return Double.parseDouble(value);
    }

    @Override
    public String convertBack(Double value) {
        return value.toString();
    }
}
