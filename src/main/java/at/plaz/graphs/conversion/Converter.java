package at.plaz.graphs.conversion;

/**
 * Created by Georg Plaz.
 */
public interface Converter<A, B>  {
    B convert(A value);
}
