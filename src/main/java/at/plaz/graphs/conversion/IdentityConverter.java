package at.plaz.graphs.conversion;

/**
 * Created by Georg Plaz.
 */
public class IdentityConverter<A> implements TwoWayConverter<A, A> {
    private static final IdentityConverter<?> instance = new IdentityConverter();
    private IdentityConverter() {

    }

    public static <A> IdentityConverter<A> instance() {
        return (IdentityConverter<A>) instance;
    }

    @Override
    public A convert(A value) {
        return value;
    }

    @Override
    public A convertBack(A value) {
        return value;
    }
}
