package at.plaz.graphs.conversion;

/**
 * Created by Georg Plaz.
 */
public class BooleanConverter implements TwoWayConverter<String, Boolean> {
    private static final BooleanConverter instance = new BooleanConverter();
    private BooleanConverter() {

    }

    public static BooleanConverter instance() {
        return instance;
    }

    @Override
    public Boolean convert(String value) {
        return Boolean.parseBoolean(value);
    }

    @Override
    public String convertBack(Boolean value) {
        return value.toString();
    }
}
