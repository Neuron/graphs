package at.plaz.graphs.conversion;

/**
 * Created by Georg Plaz.
 */
public class IntegerConverter implements TwoWayConverter<String, Integer> {
    private static final IntegerConverter instance = new IntegerConverter();
    private IntegerConverter() {

    }

    public static IntegerConverter instance() {
        return instance;
    }

    @Override
    public Integer convert(String value) {
        return Integer.parseInt(value);
    }

    @Override
    public String convertBack(Integer value) {
        return value.toString();
    }
}
