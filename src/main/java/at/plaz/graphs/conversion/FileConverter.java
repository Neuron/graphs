package at.plaz.graphs.conversion;

import java.io.File;

/**
 * Created by Georg Plaz.
 */
public class FileConverter implements TwoWayConverter<String, File> {
    private static final FileConverter instance = new FileConverter();
    private FileConverter() {

    }

    public static FileConverter instance() {
        return instance;
    }

    @Override
    public File convert(String value) {
        return new File(value);
    }

    @Override
    public String convertBack(File value) {
        return value.getPath();
    }
}
