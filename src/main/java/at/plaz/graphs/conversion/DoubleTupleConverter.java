package at.plaz.graphs.conversion;

import at.plaz.tuples.DefaultDoubleTupleF;
import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public class DoubleTupleConverter implements TwoWayConverter<String, DoubleTuple> {
    private static final DoubleTupleConverter instance = new DoubleTupleConverter();

    private DoubleTupleConverter() {

    }

    public static DoubleTupleConverter instance() {
        return instance;
    }

    @Override
    public DoubleTuple convert(String value) {
        return new DefaultDoubleTupleF(value);
    }

    @Override
    public String convertBack(DoubleTuple value) {
        return value.toString();
    }
}
