package at.plaz.graphs;

import at.plaz.graphs.value.StorableNode;

/**
 * Created by Georg Plaz.
 */
public class GenericNodeAdapter<N extends Node> extends AbstractNodeAdapter<N> {
    public GenericNodeAdapter(String nodeKey, Class<N> nodeClass, NodeFactory<N> nodeFactory) {
        super(nodeKey, nodeClass, nodeFactory);
    }

    @Override
    public void addStorables(N node, StorableNode storables) {
        storables.addDoubleTuple("location", node.location());
        storables.addBoolean("is_fixed", node.fixed());
    }
}
