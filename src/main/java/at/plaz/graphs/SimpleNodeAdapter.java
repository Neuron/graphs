package at.plaz.graphs;

/**
 * Created by Georg Plaz.
 */
public class SimpleNodeAdapter extends GenericNodeAdapter<SimpleNode> {
    public SimpleNodeAdapter() {
        super("simple_node", SimpleNode.class, SimpleNode::new);
    }
}
