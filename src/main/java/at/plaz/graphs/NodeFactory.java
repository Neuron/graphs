package at.plaz.graphs;

import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public interface NodeFactory<N extends Node> {
    N createNode(DoubleTuple center);
}
