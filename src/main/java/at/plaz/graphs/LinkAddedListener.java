package at.plaz.graphs;

/**
 * Created by Georg Plaz.
 */
public interface LinkAddedListener<N extends Node, L extends Link<N>> {
    void added(L link);
}
