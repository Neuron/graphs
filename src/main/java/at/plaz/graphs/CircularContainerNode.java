package at.plaz.graphs;

import at.plaz.graphs.forces.CircularContainerForce;
import at.plaz.graphs.forces.Locatable;
import at.plaz.graphs.forces.LocatableForceReceiver;
import at.plaz.graphs.value.DoubleProperty;
import at.plaz.tuples.DoubleTuple;

/**
 * Created by Georg Plaz.
 */
public class CircularContainerNode extends SimpleNode{
    private CircularContainerForce<Locatable, LocatableForceReceiver> nodeForce;

    public CircularContainerNode(DoubleTuple center, double radius, double slope) {
        super(center);
        nodeForce = new CircularContainerForce<>(this, radius, slope);
        fixed().set(true);
    }

    @Override
    public Force<?> createDefaultForce(Graph<?, ?> graph) {
        return nodeForce;
    }

    public DoubleProperty radius() {
        return nodeForce.radius();
    }

    public DoubleProperty slope() {
        return nodeForce.slope();
    }
}
