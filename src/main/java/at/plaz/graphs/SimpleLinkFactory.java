package at.plaz.graphs;

/**
 * Created by Georg Plaz.
 */
public class SimpleLinkFactory<N extends Node> implements LinkFactory<N, SimpleLink<N>> {
    private static final SimpleLinkFactory<?> singleton = new SimpleLinkFactory();
    
    public static <N extends Node> SimpleLinkFactory<N> singleton() {
        return (SimpleLinkFactory<N>) singleton;
    }

    @Override
    public SimpleLink<N> createLink(N firstNode, N secondNode) {
        return new SimpleLink<>(firstNode, secondNode);
    }
}
