package at.plaz.graphs;

/**
 * Created by Georg Plaz.
 */
public interface LinkFactory<N extends Node, L extends Link<N>> {
    L createLink(N firstNode, N secondNode);
}
